/**
 * Creates a new LanguageString
 * @class LanguageString
 */
function LanguageString() {
  this.default = undefined
  this.strings = {}
  this.locale = undefined
}

/**
 * Add a new locale string
 * @method
 * @param {string} locale The name of the locale
 * @param {string} strings The translated string
 */
LanguageString.prototype.add = function(locale, string) {
  if (this.default === undefined)
    this.default = string
  this.strings[locale] = string
}

/**
 * Get the string for the specified locale
 * @method
 * @param {string} [locale] If unspecified, returns a localized string if {@link LanguageString#setLocale locale} is defined or the default string if not
 * @return {string} The string matching the locale
 */
LanguageString.prototype.get = function(locale) {
  if ((locale === undefined && this.locale !== undefined)
      || (locale !== undefined && this.strings[locale] === undefined)) {
    locale = this.locale
  }
  if (locale !== undefined && this.strings[locale] !== undefined) {
    return this.strings[locale]
  } else {
    return this.default || ""
  }
}

/**
 * An alias to the {@link LanguageString#get} method
 * @method
 */
LanguageString.prototype.toString = LanguageString.prototype.get

/**
 * Returns a JSON representation of the language string
 * @method
 * @returns {Object.<string, string>|string} Either an object representing the translated strings or a single string if no locale is defined
 */
LanguageString.prototype.toJSON = function() {
  if (Object.keys(this.strings).length < 1) {
    return this.default || ""
  } else {
    return this.strings
  }
}

/**
 * Update the default locale used
 * @method
 * @param {string} locale
 */
LanguageString.prototype.setLocale = function(locale) {
  this.locale = locale
}

/**
 * Get the default locale
 * @method
 * @returns {string} locale
 */
LanguageString.prototype.getLocale = function() {
  return this.locale
}

/**
 * Create a new LanguageString object defined by the object sent as parameter
 * @method
 * @param {Object.<string, string>|string} json Either an object representing the translated strings or a single string if no translation is available
 * @returns {LanguageString}
 */
LanguageString.fromJSON = function(json = {}) {
  const ls = new LanguageString()
  if (typeof json === 'string') {
    ls.default = json
  } else {
    for (const [key, value] of Object.entries(json)) {
      ls.add(key, value)
    }
  }
  return ls
}

module.exports = LanguageString
