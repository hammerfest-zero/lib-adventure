const optionNames = [
  "mirror", "nightmare", "ninja", "bombexpert", "boost",
  "mirrormulti", "nightmaremulti", "lifesharing", "bombcontrol",
  "kickcontrol", "soccerbomb"
]

function GameOptions(_default = false) {
  optionNames.forEach(opt => this[opt] = _default)
  this.default = _default
}

GameOptions.prototype.toJSON = function() {
  const json = {default: this.default}
  optionNames
    .filter(opt => this[opt] != this.default)
    .forEach(opt => json[opt] = this[opt])
  return json
}

GameOptions.fromJSON = function(json = {}) {
  const opts = new GameOptions()
  opts.default = json.default !== undefined ? Boolean(json.default) : opts.default
  optionNames.forEach(opt => opts[opt] = (json[opt] !== undefined ? json[opt] : opts.default))
  return opts
}

module.exports = GameOptions
