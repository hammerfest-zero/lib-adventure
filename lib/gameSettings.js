function GameSettings() {
  this.graphics = {
    skins: [1, 2, 3, 4, 5, 6],
    portals_color: "blue"
  }
  this.bombs = 2
  this.lives = 6
}

GameSettings.prototype.toJSON = function() {
  return {bombs: this.bombs,
          lives: this.lives,
          graphics: {
            skins: this.graphics.skins,
            portals_color: this.graphics.portals_color
          }
         }
}

GameSettings.fromJSON = function(json = {}) {
  const gs = new GameSettings()

  if (!json.graphics)
    json.graphics = {}
  gs.graphics.skins = json.graphics.skins || gs.graphics.skins
  gs.graphics.portals_color = json.graphics.portals_color || gs.graphics.portals_color
  gs.bombs = json.bombs || gs.bombs
  gs.lives = json.lives || gs.lives
  return gs
}

module.exports = GameSettings
