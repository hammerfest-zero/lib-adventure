const Tag = require('./tag.js')
const Way = require('./way.js')

/**
 * A collection of all the {@link Tag}s and {@link Way}s used in the adventure
 * @class
 */
function Links() {
  this.tags = []
  this.ways = []
}

/**
 * Add a tag
 * @method
 * @param {Tag} tag
 */
Links.prototype.addTag = function(tag) {
  this.tags.push(tag)
}

/**
 * Add a way
 * @method
 * @param {Way} way
 */
Links.prototype.addWay = function(way) {
  this.ways.push(way)
}

/**
 * Exports the links as an XML string that the game can use
 * @method
 * @returns {string}
 */
Links.prototype.toXML = function() {
  return '<links><tags>'
    + this.tags.reduce((acc, tag) => acc + tag.toXML(), '')
    + '</tags><ways>'
    + this.ways.reduce((acc, way) => acc + way.toXML(), '')
    + '</ways></links>'
}

/**
 * Exports the links as a JSON
 * @method
 * @returns {Object}
 */
Links.prototype.toJSON = function() {
  return {tags: this.tags.map(tag => tag.toJSON()),
          ways: this.ways.map(way => way.toJSON())}
}

/**
 * Creates a new Links from a JSON.
 * @method
 * @param {Object} json
 * @returns {Links}
 */
Links.fromJSON = function(json = {"tags": [], "ways": []}) {
  const links = new Links()

  json.tags.forEach(tag => links.addTag(Tag.fromJSON(tag)))
  json.ways.forEach(way => links.addWay(Way.fromJSON(way)))
  return links
}

module.exports = Links
