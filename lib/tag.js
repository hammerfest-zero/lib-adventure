/**
 * Creates a new tag
 * @class
 * @param {string} name The name of the tag, as it will be referred as in the {@link Portal} class
 * @param {string} did The levels set to use. Must match one defined in the {@link Levels} class.
 * @param {number} lid The level id within the leven set. Starts from 0
 */
function Tag(name, did, lid) {
  this.name = name
  this.did = did
  this.lid = lid
}

/**
 * Exports the tag as an XML string that the game can use
 * @method
 * @returns {string}
 */
Tag.prototype.toXML = function() {
  return '<tag name="' + this.name
    + '" did="' + this.did
    + '" lid="' + this.lid
    + '"/>'
}

/**
 * Exports the tag as a JSON
 * @method
 * @returns {Object}
 */
Tag.prototype.toJSON = function() {
  return {name: this.name,
          did: this.did,
          lid: this.lid}
}

/**
 * Creates a new Tag from a JSON
 * @method
 * @param {Object} json
 * @returns {Tag}
 */
Tag.fromJSON = function(json) {
  return new Tag(json.name, json.did, json.lid)
}

module.exports = Tag
