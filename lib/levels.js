/**
 * This class represent all the level sets available while in game. The first set will be the adventure entry point
 * @class
 */
function Levels() {
  this.sets = {}
}

/**
 * Exports the levels as a JSON
 * @method
 * @returns {Object}
 */
Levels.prototype.toJSON = function() {
  return this.sets
}

/**
 * Creates a new Levels object from a JSON
 * @method
 * @param {Object} json
 * @return {Levels}
 */
Levels.fromJSON = function(json = {}) {
  const levels = new Levels()
  for (const [name, value] of Object.entries(json)) {
    levels.sets[name] = value
  }
  return levels
}

module.exports = Levels
