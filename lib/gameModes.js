const modeNames = [
  "solo", "multicoop", "bossrush",
  "tutorial",
  "timeattack", "multitime",
  "soccer"
]

function GameModes(_default = false) {
  modeNames.forEach(mod => this[mod] = _default)
  this.default = _default
}

GameModes.prototype.toJSON = function() {
  const json = {default: this.default}
  modeNames
    .filter(mod => this[mod] != this.default)
    .forEach(mod => json[mod] = this[mod])
  return json
}

GameModes.fromJSON = function(json = {}) {
  const mods = new GameModes()
  mods.default = json.default !== undefined ? Boolean(json.default) : mods.default
  modeNames.forEach(mod => mods[mod] = (json[mod] !== undefined ? json[mod] : mods.default))
  return mods
}

module.exports = GameModes
