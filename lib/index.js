const fs = require('fs').promises

const Links = require('./links.js')
const Portal = require('./portal.js')
const Tag = require('./tag.js')
const Way = require('./way.js')
const Levels = require('./levels.js')
const GameModes = require('./gameModes.js')
const GameOptions = require('./gameOptions.js')
const GameSettings = require('./gameSettings.js')
const LanguageString = require('./languageString.js')

function Adventure(type = "adventure") {
  this.name = new LanguageString()
  this.description = new LanguageString()
  this.images = []
  this.engine_version = "1.0"
  this.version = "1.0"
  this.author = ""
  this.type = type
  this.levels = new Levels()
  this.links = new Links()
  this.game_modes = new GameModes()
  this.game_options = new GameOptions()
  this.game = new GameSettings()
}

Adventure.prototype.toJSON = function() {
  return {name: this.name.toJSON(),
          description: this.description.toJSON(),
          images: this.images,
          engine_version: this.engine_version,
          version: this.version,
          author: this.author,
          type: this.type,
          levels: this.levels.toJSON(),
          links: this.links.toJSON(),
          game_modes: this.game_modes.toJSON(),
          game_options: this.game_options.toJSON(),
          game: this.game.toJSON()
         }
}

Adventure.fromJSON = function(json) {
  const adv = new Adventure()

  adv.name = LanguageString.fromJSON(json.name) || adv.name
  adv.description = LanguageString.fromJSON(json.description) || adv.description
  adv.images = json.images || adv.images
  adv.engine_version = json.engine_version || adv.engine_version
  adv.version = json.version || adv.version
  adv.author = json.author || adv.author
  adv.type = json.type || adv.type
  adv.levels = Levels.fromJSON(json.levels)
  adv.links = Links.fromJSON(json.links)
  adv.game_modes = GameModes.fromJSON(json.game_modes)
  adv.game_options = GameOptions.fromJSON(json.game_options)
  adv.game = GameSettings.fromJSON(json.game)
  return adv
}

Adventure.fromFile = async function(path, cb) {
  return Adventure.fromJSON(JSON.parse(await fs.readFile(path, 'utf8')))
}

Adventure.Links = Links
Adventure.Portal = Portal
Adventure.Tag = Tag
Adventure.Way = Way
Adventure.Levels = Levels
Adventure.GameModes = GameModes
Adventure.GameOptions = GameOptions
Adventure.GameSettings = GameSettings
Adventure.LanguageString = LanguageString

module.exports = Adventure
