/**
 * Creates a new Portal
 * @class
 * @param {string} tag The name of the tag. Must match a name defined as a {@link Tag}
 * @param {number} pid The portal id in the level. Ids start from 0 from left to right, and top to bottom
 * @param {number} [offset=0] The level offset from the tag
 */
function Portal(tag, pid, offset = 0) {
  this.tag = tag
  this.pid = pid
  this.offset = offset
}

/**
 * Exports the portal as a string
 * @method
 * @returns {string}
 */
Portal.prototype.toString = function() {
  return this.tag
    + (this.offset ? '+' + this.offset : '')
    + '(' + this.pid + ')'
}

/**
 * Creates a new Portal from a string representation <br/>
 * Format is <code>tag[+offset](pid)</code>
 * @method
 * @param {string}
 * @returns {Portal|undefined}
 */
Portal.fromString = function(str) {
  const split = /^(\w+)(\+\d+)?(?:\((-?\d+)\))?$/.exec(str)
  if (!split) {
    console.error('Parsing error on portal "' + str + '"')
    return undefined
  }
  return new Portal(split[1], parseInt(split[3]) || 0, parseInt(split[2]) || 0)
}

/**
 * Exports the portal as a JSON
 * @method
 * @returns {Object}
 */
Portal.prototype.toJSON = function() {
  const json = {tag: this.tag,
                pid: this.pid}
  if (this.offset)
    json.offset = this.offset
  return json
}

/**
 * Creates a new Portal from a JSON. If a string is used, then the method {@link Portal.fromString} will be used instead
 * @method
 * @param {Object|string} json
 * @returns {Portal|undefined}
 */
Portal.fromJSON = function(json) {
  if (typeof json === 'string')
    return Portal.fromString(json)
  return new Portal(json.tag, json.pid, json.offset)
}

module.exports = Portal
