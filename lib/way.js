const Portal = require('./portal.js')

/**
 * Creates a new way, linking two portals together
 * @class
 * @param {Portal} from The input portal
 * @param {Portal} to The output portal
 * @param {boolean} [twoways=false] If set to <code>true</code> the portal can be used back and forth
 */
function Way(from, to, twoways = false) {
  this.from = from
  this.to = to
  this.twoways = Boolean(twoways)
}

/**
 * Exports the way as an XML string that the game can use
 * @method
 * @returns {string}
 */
Way.prototype.toXML = function() {
  return '<'
    + (this.twoways ? 'twoway' : 'oneway')
    + ' from="' + this.from
    + '" to="' + this.to
    + '"/>'
}

/**
 * Exports the way as a JSON
 * @method
 * @returns {Object}
 */
Way.prototype.toJSON = function() {
  const json = {from: this.from.toJSON(),
                to: this.to.toJSON()}
  if (this.twoways)
    json.twoways = this.twoways
  return json
}

/**
 * Creates a new Way from a JSON
 * @method
 * @param {Object} json
 * @returns {Way}
 */
Way.fromJSON = function(json) {
  return new Way(Portal.fromJSON(json.from),
                 Portal.fromJSON(json.to),
                 json.twoways || false)
}

module.exports = Way
