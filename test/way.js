const test = require('ava')
const Adventure = require('..')

// mocking the Portal methods used in this tests
Adventure.Portal.prototype.toString = function() {
  return "__placeholder__"
}

Adventure.Portal.prototype.toJSON = function() {
  return {"__placeholder__": true}
}
// end of mocking declaration

const p1 = new Adventure.Portal()
const p2 = new Adventure.Portal()

test('Way()', t => {
  const way = new Adventure.Way(p1, p2, true)
  t.is(way.from, p1)
  t.is(way.to, p2)
  t.true(way.twoways)
})

test('Way() - default arguments', t => {
  const way = new Adventure.Way(p1, p2)
  t.is(way.from, p1)
  t.is(way.to, p2)
  t.false(way.twoways)
})

test('Way.prototype.toXML - one way', t => {
  const way = new Adventure.Way(p1, p2)
  t.is(way.toXML(), '<oneway from="__placeholder__" to="__placeholder__"/>')
})

test('Way.prototype.toXML - two ways', t => {
  const way = new Adventure.Way(p1, p2, true)
  t.is(way.toXML(), '<twoway from="__placeholder__" to="__placeholder__"/>')
})

test('Way.prototype.toJSON - one way', t => {
  const way = new Adventure.Way(p1, p2)
  t.deepEqual(way.toJSON(), {"from": {"__placeholder__": true},
                             "to": {"__placeholder__": true}})
})

test('Way.prototype.toJSON - two ways', t => {
  const way = new Adventure.Way(p1, p2, true)
  t.deepEqual(way.toJSON(), {"from": {"__placeholder__": true},
                             "to": {"__placeholder__": true},
                             "twoways": true})
})

test('Way.fromJSON - one way', t => {
  const way = Adventure.Way.fromJSON({"from": {},
                                      "to": {}})
  t.true(way.from instanceof Adventure.Portal)
  t.true(way.to instanceof Adventure.Portal)
  t.false(way.twoways)
})

test('Way.fromJSON - two ways', t => {
  const way = Adventure.Way.fromJSON({"from": {},
                                      "to": {},
                                      "twoways": true})
  t.true(way.from instanceof Adventure.Portal)
  t.true(way.to instanceof Adventure.Portal)
  t.true(way.twoways)
})
