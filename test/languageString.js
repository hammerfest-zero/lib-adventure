const test = require('ava')
const Adventure = require('..')

test('LanguageString()', t => {
  const ls = new Adventure.LanguageString()
  t.is(ls.default, undefined)
  t.deepEqual(ls.strings, {})
  t.is(ls.locale, undefined)
})

test('LanguageString.prototype.add', t => {
  const ls = new Adventure.LanguageString()
  ls.add("fr", "Clé des apprentis")
  ls.add("en", "Key of the apprentice")
  t.is(ls.default, "Clé des apprentis")
  t.deepEqual(ls.strings, {"fr": "Clé des apprentis",
                           "en": "Key of the apprentice"})
  t.is(ls.locale, undefined)
})

test('LanguageString.fromJSON - string', t => {
  const ls = Adventure.LanguageString.fromJSON("Clé des apprentis")
  t.is(ls.default, "Clé des apprentis")
  t.deepEqual(ls.strings, {})
  t.is(ls.locale, undefined)
})

test('LanguageString.fromJSON - json', t => {
  const ls = Adventure.LanguageString.fromJSON({"fr": "Clé des apprentis",
                                                "en": "Key of the apprentice",
                                                "es": "Llave de los Aprendices"})
  t.is(ls.default, "Clé des apprentis")
  t.deepEqual(ls.strings, {"fr": "Clé des apprentis",
                           "en": "Key of the apprentice",
                           "es": "Llave de los Aprendices"})
  t.is(ls.locale, undefined)
})

test('LanguageString.prototype.setLocale && getLocale ', t => {
  const ls = new Adventure.LanguageString()
  ls.setLocale("fr")
  t.is(ls.getLocale(), "fr")
})

test('LanguageString.prototype.get - from json', t => {
  const ls = Adventure.LanguageString.fromJSON({"fr": "Clé des apprentis",
                                                "en": "Key of the apprentice",
                                                "es": "Llave de los Aprendices"})
  t.is(ls.get("fr"), "Clé des apprentis")
  t.is(ls.get("en"), "Key of the apprentice")
  t.is(ls.get("es"), "Llave de los Aprendices")
  t.is(ls.get("other"), "Clé des apprentis")
  t.is(ls.get(), "Clé des apprentis")
})

test('LanguageString.prototype.get - from string', t => {
  const ls = Adventure.LanguageString.fromJSON("Clé des apprentis")
  t.is(ls.get("fr"), "Clé des apprentis")
  t.is(ls.get("en"), "Clé des apprentis")
  t.is(ls.get(), "Clé des apprentis")
})

test('LanguageString.prototype.get - with valid locale', t => {
  const ls = Adventure.LanguageString.fromJSON({"fr": "Clé des apprentis",
                                                "en": "Key of the apprentice",
                                                "es": "Llave de los Aprendices"})
  ls.setLocale("en")
  t.is(ls.get("fr"), "Clé des apprentis")
  t.is(ls.get("en"), "Key of the apprentice")
  t.is(ls.get("es"), "Llave de los Aprendices")
  t.is(ls.get("other"), "Key of the apprentice")
  t.is(ls.get(), "Key of the apprentice")
})

test('LanguageString.prototype.get - with invalid locale', t => {
  const ls = Adventure.LanguageString.fromJSON({"fr": "Clé des apprentis",
                                                "en": "Key of the apprentice",
                                                "es": "Llave de los Aprendices"})
  ls.setLocale("other")
  t.is(ls.get("fr"), "Clé des apprentis")
  t.is(ls.get("en"), "Key of the apprentice")
  t.is(ls.get("es"), "Llave de los Aprendices")
  t.is(ls.get("other"), "Clé des apprentis")
  t.is(ls.get(), "Clé des apprentis")
})

test('LanguageString.prototype.toJSON - json', t => {
  const ls = Adventure.LanguageString.fromJSON({"fr": "Clé des apprentis",
                                                "en": "Key of the apprentice",
                                                "es": "Llave de los Aprendices"})
  t.deepEqual(ls.toJSON(), {"fr": "Clé des apprentis",
                            "en": "Key of the apprentice",
                            "es": "Llave de los Aprendices"})
})

test('LanguageString.prototype.toJSON - string', t => {
  const ls = Adventure.LanguageString.fromJSON("Clé des apprentis")
  t.is(ls.toJSON(), "Clé des apprentis")
})

test('LanguageString.prototype.toJSON - empty', t => {
  const ls = new Adventure.LanguageString()
  t.is(ls.toJSON(), "")
})
