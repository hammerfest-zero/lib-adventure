const test = require('ava')
const Adventure = require('..')


test('GameOptions()', t => {
  const options = new Adventure.GameOptions()
  t.false(options.mirror)
  t.false(options.nightmare)
  t.false(options.ninja)
  t.false(options.bombexpert)
  t.false(options.boost)
  t.false(options.mirrormulti)
  t.false(options.nightmaremulti)
  t.false(options.lifesharing)
  t.false(options.bombcontrol)
  t.false(options.kickcontrol)
  t.false(options.soccerbomb)
  t.false(options.default)
})

test('GameOptions.prototype.toJSON - empty', t => {
  const options = new Adventure.GameOptions()
  t.deepEqual(options.toJSON(), {"default": false})
})

test('GameOptions.prototype.toJSON - with values', t => {
  const options = new Adventure.GameOptions()
  options.boost = true
  options.nightmare = true
  options.ninja = true
  options.bombexpert = true
  options.mirror = true
  options.default = true
  t.deepEqual(options.toJSON(), {"mirrormulti": false,
                                 "nightmaremulti": false,
                                 "lifesharing": false,
                                 "bombcontrol": false,
                                 "kickcontrol": false,
                                 "soccerbomb": false,
                                 "default": true})
})

test('GameOptions.fromJSON - empty', t => {
  const options = Adventure.GameOptions.fromJSON()
  t.false(options.mirror)
  t.false(options.nightmare)
  t.false(options.ninja)
  t.false(options.bombexpert)
  t.false(options.boost)
  t.false(options.mirrormulti)
  t.false(options.nightmaremulti)
  t.false(options.lifesharing)
  t.false(options.bombcontrol)
  t.false(options.kickcontrol)
  t.false(options.soccerbomb)
  t.false(options.default)
})

test('GameOptions.fromJSON - with values', t => {
  const options = Adventure.GameOptions.fromJSON({"mirrormulti": false,
                                                  "nightmaremulti": false,
                                                  "lifesharing": false,
                                                  "bombcontrol": false,
                                                  "kickcontrol": false,
                                                  "soccerbomb": false,
                                                  "default": true})
  t.true(options.mirror)
  t.true(options.nightmare)
  t.true(options.ninja)
  t.true(options.bombexpert)
  t.true(options.boost)
  t.false(options.mirrormulti)
  t.false(options.nightmaremulti)
  t.false(options.lifesharing)
  t.false(options.bombcontrol)
  t.false(options.kickcontrol)
  t.false(options.soccerbomb)
  t.true(options.default)
})
