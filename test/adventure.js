const path = require('path')
const test = require('ava')
const Adventure = require('..')
const FILE_PATH = path.join(__dirname, '../examples/adventure.json')
const FILE_CONTENT = require(FILE_PATH)

// mocking necessary jsonify methods
Adventure.Links.prototype.toJSON = function() {
  return {"__placeholder__": "links"}
}

Adventure.Levels.prototype.toJSON = function() {
  return {"__placeholder__": "levels"}
}

Adventure.GameModes.prototype.toJSON = function() {
  return {"__placeholder__": "modes"}
}

Adventure.GameOptions.prototype.toJSON = function() {
  return {"__placeholder__": "options"}
}

Adventure.GameSettings.prototype.toJSON = function() {
  return {"__placeholder__": "settings"}
}

Adventure.LanguageString.prototype.toJSON = function() {
  return this.default || ""
}
// end of mocking declaration

test('Adventure()', t => {
  const adv = new Adventure()
  t.is(adv.name.toJSON(), "")
  t.is(adv.description.toJSON(), "")
  t.deepEqual(adv.images, [])
  t.is(adv.engine_version, "1.0")
  t.is(adv.version, "1.0")
  t.is(adv.author, "")
  t.is(adv.type, "adventure")
  t.true(adv.levels instanceof Adventure.Levels)
  t.true(adv.links instanceof Adventure.Links)
  t.true(adv.game_modes instanceof Adventure.GameModes)
  t.true(adv.game_options instanceof Adventure.GameOptions)
  t.true(adv.game instanceof Adventure.GameSettings)
})

test('Adventure.toJSON - empty', t => {
  const adv = new Adventure()
  t.deepEqual(adv.toJSON(), {"name": "",
                             "description": "",
                             "version": "1.0",
                             "engine_version": "1.0",
                             "images": [],
                             "author": "",
                             "type": "adventure",
                             "levels": {"__placeholder__": "levels"},
                             "links": {"__placeholder__": "links"},
                             "game_modes": {"__placeholder__": "modes"},
                             "game_options": {"__placeholder__": "options"},
                             "game": {"__placeholder__": "settings"}})
})

test('Adventure.fromJSON - empty', t => {
  const adv = Adventure.fromJSON({})
  t.is(adv.name.toJSON(), "")
  t.is(adv.description.toJSON(), "")
  t.deepEqual(adv.images, [])
  t.is(adv.engine_version, "1.0")
  t.is(adv.version, "1.0")
  t.is(adv.author, "")
  t.is(adv.type, "adventure")
  t.true(adv.levels instanceof Adventure.Levels)
  t.true(adv.links instanceof Adventure.Links)
  t.true(adv.game_modes instanceof Adventure.GameModes)
  t.true(adv.game_options instanceof Adventure.GameOptions)
  t.true(adv.game instanceof Adventure.GameSettings)
})

test('Adventure.fromJSON - real file', t => {
  const adv = Adventure.fromJSON(FILE_CONTENT)
  t.is(adv.name.toJSON(), "Hammerfest")
  t.is(adv.description.toJSON(), "The original game, without any modification")
  t.is(typeof adv.images[0], "string")
  t.is(typeof adv.images[1], "string")
  t.is(adv.engine_version, "1.0")
  t.is(adv.version, "1.0")
  t.is(adv.author, "Motion-Twin")
  t.is(adv.type, "adventure")
  t.true(adv.levels instanceof Adventure.Levels)
  t.true(adv.links instanceof Adventure.Links)
  t.true(adv.game_modes instanceof Adventure.GameModes)
  t.true(adv.game_options instanceof Adventure.GameOptions)
  t.true(adv.game instanceof Adventure.GameSettings)
})

test('Adventure.fromFile - real file', async t => {
  const adv = await Adventure.fromFile(FILE_PATH)
  t.is(adv.name.toJSON(), "Hammerfest")
  t.is(adv.description.toJSON(), "The original game, without any modification")
  t.is(typeof adv.images[0], "string")
  t.is(typeof adv.images[1], "string")
  t.is(adv.engine_version, "1.0")
  t.is(adv.version, "1.0")
  t.is(adv.author, "Motion-Twin")
  t.is(adv.type, "adventure")
  t.true(adv.levels instanceof Adventure.Levels)
  t.true(adv.links instanceof Adventure.Links)
  t.true(adv.game_modes instanceof Adventure.GameModes)
  t.true(adv.game_options instanceof Adventure.GameOptions)
  t.true(adv.game instanceof Adventure.GameSettings)
})
