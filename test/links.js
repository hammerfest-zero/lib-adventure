const test = require('ava')
const Adventure = require('..')

// mocking the Tag and Way classes
Adventure.Tag = function() {}
Adventure.Way = function() {}

Adventure.Tag.prototype.toXML = function() {
  return "__placeholder_tag__"
}

Adventure.Way.prototype.toXML = function() {
  return "__placeholder_way__"
}

Adventure.Tag.prototype.toJSON = function() {
  return {"__placeholder__": "tag"}
}

Adventure.Way.prototype.toJSON = function() {
  return {"__placeholder__": "way"}
}
// end of mocking declaration

const tag = new Adventure.Tag()
const way = new Adventure.Way()

test('Links()', t => {
  const links = new Adventure.Links()
  t.true(links.tags instanceof Array)
  t.true(links.ways instanceof Array)
})

test('Links.prototype.addTag', t => {
  const links = new Adventure.Links()
  links.addTag(tag)
  t.is(links.tags[0], tag)
})

test('Links.prototype.addWay', t => {
  const links = new Adventure.Links()
  links.addWay(way)
  t.is(links.ways[0], way)
})

test('Links.prototype.toXML - empty', t => {
  const links = new Adventure.Links()
  t.is(links.toXML(), '<links><tags></tags><ways></ways></links>')
})

test('Links.prototype.toXML - filled', t => {
  const links = new Adventure.Links()
  links.addTag(tag)
  links.addWay(way)
  t.is(links.toXML(), '<links><tags>__placeholder_tag__</tags><ways>__placeholder_way__</ways></links>')
})

test('Links.prototype.toJSON - empty', t => {
  const links = new Adventure.Links()
  t.deepEqual(links.toJSON(), {"tags": [],
                               "ways": []})
})

test('Links.prototype.toJSON - filled', t => {
  const links = new Adventure.Links()
  links.addTag(tag)
  links.addWay(way)
  t.deepEqual(links.toJSON(), {"tags": [{"__placeholder__": "tag"}],
                               "ways": [{"__placeholder__": "way"}]})
})

test('Links.prototype.fromJSON - empty', t => {
  const links = Adventure.Links.fromJSON()
  t.true(links.tags instanceof Array)
  t.true(links.ways instanceof Array)
})
