const test = require('ava')
const Adventure = require('..')

// mocking the console.error function
console.error = function() {}

test('Portal()', t => {
  const portal = new Adventure.Portal("main01", 1, 1)
  t.is(portal.tag, "main01")
  t.is(portal.pid, 1)
  t.is(portal.offset, 1)
})

test('Portal() - default arguments', t => {
  const portal = new Adventure.Portal("main01", 1)
  t.is(portal.tag, "main01")
  t.is(portal.pid, 1)
  t.is(portal.offset, 0)
})

test('Portal.prototype.toString - full', t => {
  const portal = new Adventure.Portal("main01", 0, 1)
  t.is(portal.toString(), "main01+1(0)")
})

test('Portal.protoype.toString - no offset', t => {
  const portal = new Adventure.Portal("main01", 0)
  t.is(portal.toString(), "main01(0)")
})

test('Portal.prototype.toString - offset 0', t => {
  const portal = new Adventure.Portal("main01", 0, 0)
  t.is(portal.toString(), "main01(0)")
})

test('Portal.prototyre.toJSON - full', t => {
  const portal = new Adventure.Portal("main01", 1, 1)
  t.deepEqual(portal.toJSON(), {"tag": "main01",
                                "pid": 1,
                                "offset": 1})
})

test('Portal.prototyre.toJSON - no offset', t => {
  const portal = new Adventure.Portal("main01", 1)
  t.deepEqual(portal.toJSON(), {"tag": "main01",
                                "pid": 1})
})

test('Portal.prototyre.toJSON - offset 0', t => {
  const portal = new Adventure.Portal("main01", 1, 0)
  t.deepEqual(portal.toJSON(), {"tag": "main01",
                                "pid": 1})
})

test('Portal.fromJSON - full', t => {
  const portal = Adventure.Portal.fromJSON({"tag": "main01", "pid": 1, "offset": 1})
  t.is(portal.tag, "main01")
  t.is(portal.pid, 1)
  t.is(portal.offset, 1)
})

test('Portal.fromJSON - no offset', t => {
  const portal = Adventure.Portal.fromJSON({"tag": "main01", "pid": 1})
  t.is(portal.tag, "main01")
  t.is(portal.pid, 1)
  t.is(portal.offset, 0)
})

test('Portal.fromString - complete', t => {
  const portal = Adventure.Portal.fromString("main01+5(1)")
  t.is(portal.tag, "main01")
  t.is(portal.pid, 1)
  t.is(portal.offset, 5)
})

test('Portal.fromString - no offset', t => {
  const portal = Adventure.Portal.fromString("main01(1)")
  t.is(portal.tag, "main01")
  t.is(portal.pid, 1)
  t.is(portal.offset, 0)
})

test('Portal.fromString - parsing error', t => {
  const portal = Adventure.Portal.fromString("main01 (1)")
  t.is(portal, undefined)
})
