const test = require('ava')
const Adventure = require('..')

test('GameSettings()', t => {
  const game = new Adventure.GameSettings()
  t.is(game.bombs, 2)
  t.is(game.lives, 6)
  t.deepEqual(game.graphics.skins, [1, 2, 3, 4, 5, 6])
  t.is(game.graphics.portals_color, "blue")
})

test('GameSettings.prototype.toJSON() - empty', t => {
  const game = new Adventure.GameSettings()
  t.deepEqual(game.toJSON(), {bombs: 2,
                              lives: 6,
                              graphics: {
                                skins: [1, 2, 3, 4, 5, 6],
                                portals_color: "blue"
                              }})
})

test('GameSettings.fromJSON() - empty', t => {
  const game = Adventure.GameSettings.fromJSON()
  t.is(game.bombs, 2)
  t.is(game.lives, 6)
  t.deepEqual(game.graphics.skins, [1, 2, 3, 4, 5, 6])
  t.is(game.graphics.portals_color, "blue")
})

test('GameSettings.fromJSON() - complete', t => {
  const game = Adventure.GameSettings.fromJSON({bombs: 1,
                                                lives: 1,
                                                graphics: {
                                                  skins: [7],
                                                  portals_color: "red"
                                                }})
  t.is(game.bombs, 1)
  t.is(game.lives, 1)
  t.deepEqual(game.graphics.skins, [7])
  t.is(game.graphics.portals_color, "red")
})
