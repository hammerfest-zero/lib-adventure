const test = require('ava')
const Adventure = require('..')

test('GameModes()', t => {
  const modes = new Adventure.GameModes()
  t.false(modes.solo)
  t.false(modes.multicoop)
  t.false(modes.bossrush)
  t.false(modes.tutorial)
  t.false(modes.timeattack)
  t.false(modes.multitime)
  t.false(modes.soccer)
  t.false(modes.default)
})

test('GameModes.prototype.toJSON() - empty', t => {
  const modes = new Adventure.GameModes()
  t.deepEqual(modes.toJSON(), {"default": false})
})

test('GameModes.prototype.toJSON - with values', t => {
  const modes = new Adventure.GameModes()
  modes.solo = true
  modes.multicoop = true
  modes.default = true
  t.deepEqual(modes.toJSON(), {"bossrush": false,
                               "tutorial": false,
                               "timeattack": false,
                               "multitime": false,
                               "soccer": false,
                               "default": true})
})

test('GameModes.fromJSON() - empty', t => {
  const modes = Adventure.GameModes.fromJSON()
  t.false(modes.solo)
  t.false(modes.multicoop)
  t.false(modes.bossrush)
  t.false(modes.tutorial)
  t.false(modes.timeattack)
  t.false(modes.multitime)
  t.false(modes.soccer)
  t.false(modes.default)
})

test('GameModes.fromJSON() - with values', t => {
  const modes = Adventure.GameModes.fromJSON({"bossrush": false,
                                              "tutorial": false,
                                              "timeattack": false,
                                              "multitime": false,
                                              "soccer": false,
                                              "default": true})
  t.true(modes.solo)
  t.true(modes.multicoop)
  t.false(modes.bossrush)
  t.false(modes.tutorial)
  t.false(modes.timeattack)
  t.false(modes.multitime)
  t.false(modes.soccer)
  t.true(modes.default)
})
