const test = require('ava')
const Adventure = require('..')

test('Levels()', t => {
  const levels = new Adventure.Levels()
  t.deepEqual(levels.sets, {})
})

test('Levels.prototype.toJSON - empty', t => {
  const levels = new Adventure.Levels()
  t.deepEqual(levels.toJSON(), {})
})

test('Levels.prototype.toJSON - filled', t => {
  const levels = new Adventure.Levels()
  levels.sets.adventure = "a"
  levels.sets.deepnight = "b"
  t.deepEqual(levels.toJSON(), {"adventure": "a",
                                "deepnight": "b"})
})

test('Levels.fromJSON - empty', t => {
  const levels = Adventure.Levels.fromJSON({})
  t.deepEqual(levels.sets, {})
})

test('Levels.fromJSON - completely filled', t => {
  const levels = Adventure.Levels.fromJSON({"adventure": "a",
                                            "deepnight": "b"})
  t.deepEqual(levels.sets, {"adventure": "a",
                            "deepnight": "b"})
})
