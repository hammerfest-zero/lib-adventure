const test = require('ava')
const Adventure = require('..')

test('Tag()', t => {
  const tag = new Adventure.Tag("main01", 0, 1)
  t.is(tag.name, "main01")
  t.is(tag.did, 0)
  t.is(tag.lid, 1)
})

test('Tag.prototype.toXML', t => {
  const tag = new Adventure.Tag("main01", 0, 1)
  t.is(tag.toXML(), '<tag name="main01" did="0" lid="1"/>')
})

test('Tag.prototype.toJSON', t => {
  const tag = new Adventure.Tag("main01", 0, 1)
  t.deepEqual(tag.toJSON(), {"name": "main01",
                             "did": 0,
                             "lid": 1})
})

test('Tag.prototype.fromJSON', t => {
  const tag = Adventure.Tag.fromJSON({"name": "main01",
                                      "did": 0,
                                      "lid":1})
  t.true(tag instanceof Adventure.Tag)
  t.is(tag.name, "main01")
  t.is(tag.did, 0)
  t.is(tag.lid, 1)
})
