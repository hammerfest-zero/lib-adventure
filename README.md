# Adventure Library

The Adventure library is used to interact with adventures files, which are json files that describe a custom adventure.  
This format is described below. A more complete description of the format is available [here](./doc/adventure.md).  
Note that even if there are specific version numbers written down there, this spec is still early draft and prone to change in the near future.


| attribute | type | mandatory | default | introduced in | description
| --- | --- | :---: | --- | :---: | --- |
| `name` | [`language string`](#language-string) | Yes | | 1.0 | The name of the adventure |
| `description` | [`language string`](#language-string) | No | | 1.0 | The description of the adventure |
| `type` | `string` | Yes | | 1.0 | Must be either `"adventure"`, `"tutorial"`, `"soccer"` or `"timeattack"` |
| `version` | `string` | No | `"1.0.0"` | 1.0 | The adventure version number |
| `engine_version` | `string` | Yes | | 1.0 | The version of the file format used. Must be `1.0` at the moment |
| `author` | `string` | No | | 1.0 | The adventure's author name |
| `images` | `string[]` | No | | 1.0 | URL of the adventure images, can be base64 URLs |
| |
| `levels` | `object` | Yes | | 1.0 | |
| |
| `links` | `object` | No | | 1.0 | Links between levels, used to manage door and portals |
| `links.tags` | `object[]` | Yes | | 1.0 | |
| `links.tags[].name` | `string` | Yes | | 1.0 | |
| `links.tags[].lid` | `number` | Yes | | 1.0 | |
| `links.tags[].did` | `string` | Yes | | 1.0 | Must match one defined in `levels` |
| `links.ways` | `object[]` | Yes | | 1.0 | |
| `links.ways[].from` | `object` | Yes | | 1.0 | |
| `links.ways[].from.tag` | `string` | Yes | | 1.0 | |
| `links.ways[].from.pid` | `number` | No | `TBD` | 1.0 | |
| `links.ways[].to` | `object` | Yes | | 1.0 | |
| `links.ways[].to.tag` | `string` | Yes | | 1.0 | |
| `links.ways[].to.pid` | `number\|null` | No | `TBD` | 1.0 | |
| `links.ways[].twoways` | `boolean` | No | `false` | 1.0 | |
| |
| `game` | `object` | No | | 1.0 | |
| `game.lives` | `number` | No | `6` | 1.0 | |
| `game.bombs` | `number` | No | `2` | 1.0 | |
| `game.mods` | `string[]` | No | | future | List of mods activated in this adventure. [See documentation](./doc/portals.md) for more details. |
| |
| `game.graphics` | `object` | No | | 1.0 | |
| `game.graphics.skins` | `number[]` | No | `[1, 2, 3, 4, 5, 6]` | 1.0 | Skins available to Igor. [See documentation](./doc/skins.md) for more details. |
| `game.graphics.portals_color` | `string` | No | `"blue"` | 1.0 | Custom portals color. [See documentation](./doc/portals.md) for more details. |
| `game.graphics.platforms` | `string[]` | No | | future | |
| `game.graphics.background` | `string[]` | No | | future | |


## Custom types
### Language String

Either a non localized string or an object of language/strings.

Here are some examples:
```js
/* Non localized french string */
"Gazon maudit"

/* The same string, available in several languages */
{
  "fr": "Gazon maudit",
  "en": "Dark Arena",
  "es": "Césped maldito"
}
```
